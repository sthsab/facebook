package com.example.facebook;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button btnLogin;
    //   int count=0;
    EditText username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin = (Button) findViewById(R.id.bt_login);

        username = (EditText) findViewById(R.id.et_email);
        password = (EditText) findViewById(R.id.et_password);

        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String email = username.getText().toString();   //type check
                String pass = password.getText().toString();

                Intent callingIntent = new Intent(MainActivity.this, DetailActivity.class);

                callingIntent.putExtra("email", email);
                callingIntent.putExtra("password", pass);

                startActivity(callingIntent);


            }

        });
    }



    @Override
    protected void onStart(){
        super.onStart();
        Toast.makeText( this, "welcome to login page", Toast.LENGTH_SHORT).show();
    }
}
